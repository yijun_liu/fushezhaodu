#include "Filter.h"

void excludeCoveredUnits(ImagePlane &plane, vector<Node> &nodes, vector<Unit> &units)
{
    for (int i = 0; i < plane.getN(); i++)
    {
        for (int j = 0; j < plane.getN(); j++)
        {
            int num = plane.units[i][j].indexOfUnit.size();
            if (num > 1)
            {
                deleteCovered(plane, plane.units[i][j], nodes, units);
            }
        }
    }
}

void deleteCovered(ImagePlane &plane, ImageUnit &imageUnit, vector<Node> &nodes, vector<Unit> &units)
{
    while (imageUnit.indexOfUnit.size() > 1)
    {
        Unit temp = units[imageUnit.indexOfUnit.front()];
        imageUnit.indexOfUnit.pop();
        if (!isCovered(temp, plane, nodes, units))
        {
            imageUnit.indexOfUnit.push(temp.getIndex());
        }
    }
}

int isCovered(Unit &unit, ImagePlane &plane, vector<Node> &nodes, vector<Unit> &units)
{
    int covered = 0;
    for (int i = 0; i < 4; i++)
    {
        Node point = nodes[unit.getNode()[i]];
        if (!isClosest(point, unit.getIndex(), plane, nodes, units))
        {
            covered = 1;
            break;
        }
    }
    return covered;
}

int isClosest(Node &point, int unitIndex, ImagePlane &plane, vector<Node> &nodes, vector<Unit> &units)
{
    int closest = 1;
    UnitIndex projectedImageUnit;
    vector<double> distance;
    vector<int> indexOfDistance;
    double d0 = plane.getD0();
    int n = plane.getN();
    projectedImageUnit.i = (int)(point.getY() / d0) + (n - 1) / 2;
    projectedImageUnit.j = (int)(point.getX() / d0) + (n - 1) / 2;
    std::queue<int> index = plane.units[projectedImageUnit.i][projectedImageUnit.j].indexOfUnit;
    while (index.size())
    {
        int temp = index.front();
        index.pop();
        Unit comporison = units[temp];
        double a, b, c, d;
        calcPlane(a, b, c, d, comporison.getNode(), nodes);
        double dist = abs(-1 * (d + a*point.getX() + b*point.getY()) / c);
        distance.push_back(dist);
        indexOfDistance.push_back(temp);
    }
    if (unitIndex != findMinDist(distance, indexOfDistance))
    {
        closest = 0;
    }
    return closest;
}

void calcPlane(double &a, double &b, double &c, double &d, int* corners, vector<Node> nodes)
{
    double x[3];
    double y[3];
    double z[3];
    for (int i = 0; i < 3; i++)
    {
        x[i] = nodes[corners[i]].getX();
        y[i] = nodes[corners[i]].getY();
        z[i] = nodes[corners[i]].getZ();
    }

    a = y[0]*z[1] - y[0]*z[2] - y[1]*z[0] + y[1]*z[2] + y[2]*z[0] - y[2]*z[1];
    b = -1*x[0]*z[1] + x[0]*z[2] + x[1]*z[0] - x[1]*z[2] - x[2]*z[0] + x[2]*z[1];
    c = x[0]*y[1] - x[0]*y[2] - x[1]*y[0] + x[1]*y[2] + x[2]*y[0] - x[2]*y[1];
    d = -1*x[0]*y[1]*z[2] + x[0]*y[2]*z[1] + x[1]*y[0]*z[2] - x[1]*y[2]*z[0] - x[2]*y[0]*z[1] + x[2]*y[1]*z[0];
}

int findMinDist(vector<double> &distance, vector<int> &indexOfDistance)
{
    double min = distance[0];
    int index = 0;
    for (int i = 1; i < distance.size(); i++)
    {
        if (distance[i] < min)
        {
            min = distance[i];
            index = i;
        }
    }

    return indexOfDistance[index];
}