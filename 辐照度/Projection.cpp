#include "Projection.h"

void projectUnit(Unit unit, ImagePlane &plane, vector<Node> &nodes, vector<Unit> &units)
{
    double d0 = plane.getD0();
    int n = plane.getN();
    ImageNode corner[4];
    for (int i = 0; i < 4; i++)
    {
        corner[i] = ImageNode(nodes.at(unit.getNode()[i]).getX(), nodes.at(unit.getNode()[i]).getY());
    }

    vector<UnitIndex> index;
    for (int i = 0; i < 4; i++)
    {
        UnitIndex loc;
        loc.i = (int)(corner[i].getY() / d0) + (n - 1) / 2;
        loc.j = (int)(corner[i].getX() / d0) + (n - 1) / 2;
        index.push_back(loc);
    }

    if (index[0].i == index[1].i && index[1].i == index[2].i && index[2].i == index[3].i &&
        index[0].j == index[1].j && index[1].j == index[2].j && index[2].j == index[3].j)
    {
        // 所有节点位于同一投影网格单元�?
        plane.units[index[0].i][index[0].j].indexOfUnit.push(unit.getIndex());
    }
    else
    {
        int minI, minJ, maxI, maxJ;
        findMinMax(minI, minJ, maxI, maxJ, index);
        double offset = plane.getSizeOfUnit() / 2;
        for (int i = minI; i <= maxI; i++)
        {
            for (int j = minJ; j <= maxJ; j++)
            {
                ImageNode unitCorner[4];
                double centerX = plane.units[i][j].getCenterX();
                double centerY = plane.units[i][j].getCenterY();
                unitCorner[0] = ImageNode(centerX - offset, centerY - offset);
                unitCorner[1] = ImageNode(centerX + offset, centerY - offset);
                unitCorner[2] = ImageNode(centerX + offset, centerY + offset);
                unitCorner[3] = ImageNode(centerX - offset, centerY + offset);

                for (int k = 0; k < 4; k++)
                {
                    if (isInTheArea(corner, unitCorner[k]))
                    {
                        plane.units[i][j].indexOfUnit.push(unit.getIndex());
                        break;
                    }
                }
            }
        }
    }
}

void findMinMax(int &minI, int &minJ, int &maxI, int &maxJ, vector<UnitIndex> index)
{
    minI = maxI = index[0].i;
    minJ = maxJ = index[0].j;
    for (int i = 1; i < 4; i++)
    {
        if (index[i].i < minI)
        {
            minI = index[i].i;
        }
        else if (index[i].i > maxI)
        {
            maxI = index[i].i;
        }

        if (index[i].j < minJ)
        {
            minJ = index[i].j;
        }
        else if (index[i].j > maxJ)
        {
            maxJ = index[i].j;
        }
    }
}

int isInTheArea(const ImageNode* corner, ImageNode point)
{
    int isInArea = 0;
    double totalDegree = 0;
    for (int i = 0; i < 3; i++)
    {
        double degree = calcDegreeBetweenVectors(corner[i], corner[i + 1], point);
        totalDegree += degree;
    }

    double degree = calcDegreeBetweenVectors(corner[3], corner[0], point);
    totalDegree += degree;
    if (abs(totalDegree - 2 * PI) < 10E-5)
    {
        isInArea = 1;
    }

    return isInArea;
}

double calcDegreeBetweenVectors(ImageNode corner1, ImageNode corner2, ImageNode point)
{
    double a1 = corner1.getX() - point.getX();
    double a2 = corner1.getY() - point.getY();
    double b1 = corner2.getX() - point.getX();
    double b2 = corner2.getY() - point.getY();

    double cosDegree = (a1*b1 + a2*b2) / (sqrt(a1*a1 + a2*a2) * sqrt(b1*b1 + b2*b2));
    double degree = acos(cosDegree);

    return degree;
}

void transformCoorSys(vector<Node> &originalNodes, vector<Node> nodes, double* eulerAngle, double distance)
{
    double theta = eulerAngle[0];   // 俯仰�?
    double gamma = eulerAngle[1];   // 横滚�?
    double psi = eulerAngle[2]; //航向�?
    
    long double matrix[3][3];
    long double cosGamma = cos(gamma), sinGamma = sin(gamma);
    long double cosTheta = cos(theta), sinTheta = sin(theta);
    long double cosPsi = cos(psi), sinPsi = sin(psi);
    matrix[0][0] = cosGamma * cosPsi + sinGamma * sinTheta * sinPsi;
    matrix[0][1] = -1 * cosGamma * sinPsi + sinGamma * sinTheta * cosPsi;
    matrix[0][2] = -1 * sinGamma * cosTheta;
    matrix[1][0] = cosTheta * sinPsi;	matrix[1][1] = cosTheta * cosPsi;
    matrix[1][2] = sinTheta;
    matrix[2][0] = sinGamma * cosPsi - cosGamma * sinTheta * sinPsi;
    matrix[2][1] = -1 * sinGamma * cosPsi - cosGamma * sinTheta * cosPsi;
    matrix[2][2] = cosGamma * cosTheta;

    for (int i = 0; i < originalNodes.size(); i++)
    {
        double originX = originalNodes[i].getX();
        double originY = originalNodes[i].getY();
        double originZ = originalNodes[i].getZ();
        double index = originalNodes[i].getIndex();

        double x = matrix[0][0] * originX + matrix[0][1] * originY + matrix[0][2] * originZ;
        double y = matrix[1][0] * originX + matrix[1][1] * originY + matrix[1][2] * originZ;
        double z = matrix[2][0] * originX + matrix[2][1] * originY + matrix[2][2] * originZ + distance;

        nodes.push_back(Node(x, y, z, index));
    }
}