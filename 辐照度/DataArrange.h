//#pragma once
#include "DataStruct.h"
class CDataArrange
{
public:
	CDataArrange(void);
	~CDataArrange(void);
private:
	char* m_DataPath;
	FILE* m_RePointer;
	FILE* m_ArrayPointer;
	FILE* m_DataFilePointer;
public:
	int SetDataPath(char* DataPath);
	int ReadZoneHead(int* NodeNum, int* ElementNum);
private:
	int m_NodesNum;
	int m_EleNum;
public:
	int ReadZoneData(double* X,double* Y,double* Z,Element* Ele,double* density,double* Temperature,double* Press);
	int StoreNeighbour(Element* Ele);
	int FindSurface(TopLayer** pTopLayer,TopLayer** pBottomLayer,Element* Ele,double* X,double* Y,double* Z);
private:
	int GetUpFace(int Index,TopLayer* pLayer,TopLayer* pLayer1,int* LayerNum,Element* Ele,double* X,double* Y,double* Z);
	void FaceEqual(Surface* Origin,Surface* Des);
	int Analysis2(Up_Down* pUpDown,Element* Ele);
	void FaceArrange(Element* pElement,Surface* pTemp);
	int CompareFace(Surface* face1, Surface* face2);
	double SolveAbsDistance(int Index,double* Xcontainer,double* Ycontainer,double* Zcontainer);
	int AddTopElement(TopLayer* pLayer,int Num,TopLayer** pTopLayer);
public:
//	int WriteVertex(int* pVertex, double* X , double* Y , double* Z , double* Tem,Element* pEle,char* path);
//	int WriteTopLayer(struct TopLayer* pTopLayer, int ElementCount,double *Xcontainer,double *Ycontainer,double *Zcontainer,double *Value,char* path);
//	int WriteArrange(int up, int down,double* X , double* Y , double* Z , double* Tem,Element* pEle);
	int GetNeighbour(int GridNum,int* pRe);
	void FindNeighbour(int Index,Element* Ele,int* NeighbourArray,int BaseAxis);
};

