#pragma once
#include "Grid.h"
#include <vector>
#include <cmath>

typedef struct
{
    int i;
    int j;
}   UnitIndex;
using std::vector;

void transformCoorSys(vector<Node> &originalNodes, vector<Node> nodes, double* eulerAngle, double distance); // 转换坐标�?
void projectUnit(Unit unit, ImagePlane &plane, vector<Node> &nodes, vector<Unit> &units);
void findMinMax(int &minI, int &minJ, int &maxI, int &maxJ, vector<UnitIndex> index);
int isInTheArea(const ImageNode* corner, ImageNode point);
double calcDegreeBetweenVectors(ImageNode corner1, ImageNode corner2, ImageNode point);