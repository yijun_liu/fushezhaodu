#include "Grid.h"

Node::Node()
{
    x = y = z = 0;
    index = 0;
}

Node::Node(double x, double y, double z, int index)
{
    this->x = x;
    this->y = y;
    this->z = z;
    this->index = index;
}

void Node::setNode(double x, double y, double z, int index)
{
    this->x = x;
    this->y = y;
    this->z = z;
    this->index = index;
}

double Node::getX()
{
    return x;
}

double Node::getY()
{
    return y;
}

double Node::getZ()
{
    return z;
}

int Node::getIndex()
{
    return index;
}

Unit::Unit()
{
    node[0] = node[1] = node[2] = node[3] = 0;
    index = 0;
}

Unit::Unit(int node1, int node2, int node3, int node4, int index, double t)
{
    this->node[0] = node1;
    this->node[1] = node2;
    this->node[2] = node3;
    this->node[3] = node4;
    this->index = index;
    this->t = t;
}

void Unit::setUnit(int node1, int node2, int node3, int node4, int index, double t)
{
    this->node[0] = node1;
    this->node[1] = node2;
    this->node[2] = node3;
    this->node[3] = node4;
    this->index = index;
    this->t = t;
}

int* Unit::getNode()
{
    return node;
}

int Unit::getIndex()
{
    return index;
}

double Unit::getT()
{
    return t;
}

ImageNode::ImageNode()
{
    x = y = 0;
}

ImageNode::ImageNode(double x, double y)
{
    this->x = x;
    this->y = y;
}

double ImageNode::getX()
{
    return x;
}

double ImageNode::getY()
{
    return y;
}

ImageUnit::ImageUnit()
{
    i = j = 0;
    center = ImageNode();
}

ImageUnit::ImageUnit(ImageNode center, int i, int j)
{
    this->center = center;
    this->i = i;
    this->j = j;
}

double ImageUnit::getCenterX()
{
    return center.getX();
}

double ImageUnit::getCenterY()
{
    return center.getY();
}

ImagePlane::ImagePlane()
{
    viewDegree = 0;
    l = d0 = 0;
    n = calcNumOfUnit(l, viewDegree, d0);
    sizeOfUnit = calcSizeOfUnit(l, viewDegree, n);
    units = NULL;
}

ImagePlane::ImagePlane(double viewDegree, double l, double d0)
{
    this->viewDegree = viewDegree;
    this->l = l;
    this->d0 = d0;
    n = calcNumOfUnit(l, viewDegree, d0);
    sizeOfUnit = calcSizeOfUnit(l, viewDegree, n);
    units = createUnits(n, sizeOfUnit);
}

void ImagePlane::setImagePlane(double viewDegree, double l, double d0)
{
    this->viewDegree = viewDegree;
    this->l = l;
    this->d0 = d0;
    n = calcNumOfUnit(l, viewDegree, d0);
    sizeOfUnit = calcSizeOfUnit(l, viewDegree, n);
    units = createUnits(n, sizeOfUnit);
}

int ImagePlane::calcNumOfUnit(double l, double viewDegree, double d0)
{
    int temp = (int)(l * viewDegree / (2 * d0));
    return 2 * temp + 1;
}

double ImagePlane::calcSizeOfUnit(double l, double viewDegree, int n)
{
    return l * viewDegree / n;
}

ImageUnit** ImagePlane::createUnits(int n, double sizeOfUnit)
{
    ImageUnit** gridUnits;
    double offset = sizeOfUnit / 2 * (n - 1);
    gridUnits = new ImageUnit*[n];
    for (int i = 0; i < n; i++)
    {
        gridUnits[i] = new ImageUnit[n];
        for (int j = 0; j < n; j++)
        {
            double centerX = sizeOfUnit * j - offset;
            double centerY = sizeOfUnit * i - offset;
            gridUnits[i][j] = ImageUnit(ImageNode(centerX, centerY), i, j);
        }
    }

    return gridUnits;
}

double ImagePlane::getD0()
{
    return d0;
}

int ImagePlane::getN()
{
    return n;
}

double ImagePlane::getSizeOfUnit()
{
    return sizeOfUnit;
}