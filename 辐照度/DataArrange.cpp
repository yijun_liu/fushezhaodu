#include "stdafx.h"
#include "string.h"
#include "math.h"
#include <stdio.h>
#include <malloc.h>
#include <stdlib.h>
#include "DataArrange.h"
CDataArrange::CDataArrange(void)
: m_DataPath(NULL)
, m_RePointer(NULL)
, m_ArrayPointer(NULL)
, m_DataFilePointer(NULL)
, m_NodesNum(0)
, m_EleNum(0)
{
	fopen_s(&m_RePointer,".\\Neibourhood.buf","w+b");
	fopen_s(&m_ArrayPointer,".\\Pointbuffer.buf","w+b");
}

CDataArrange::~CDataArrange(void)
{
	if(m_RePointer)
	{
		fclose(m_RePointer);
	}
	if(m_ArrayPointer)
	{
		fclose(m_ArrayPointer);
	}
	if(m_DataFilePointer)
	{
		fclose(m_DataFilePointer);
	}
	//	CFile::Remove(_T(".\\Pointbuffer.buf"));
	//	CFile::Remove(_T(".\\Neibourhood.buf"));
}

//
//设置输入路径
//datapath为完整路径名
int CDataArrange::SetDataPath(char* DataPath)
{
	if(strlen(DataPath)==0)
	{
		return -2;//空字符串
	}
	m_DataPath=DataPath;	
	if(fopen_s(&m_DataFilePointer,m_DataPath,"r"))
	{
		return -1;//打开文件失败
	}
	return 0;//打开文件正常
}
//
//读取文件头信息，包括节点数目和网格数目
//nodenum为节点数，elementnum为网格数，通过指针参数传递
int CDataArrange::ReadZoneHead(int* NodeNum, int* ElementNum)
{
	int Num_buffer;
	int result=0;
	while(!result)
	{
		result=fscanf_s(m_DataFilePointer,"%d",&Num_buffer);
		int Seek_result=fseek(m_DataFilePointer,sizeof(char),SEEK_CUR);
		if(Seek_result)
		{
			return 0;
		}
	}
	*NodeNum=Num_buffer;
	m_NodesNum=Num_buffer;
	result=0;
	while(!result)
	{
		result=fscanf_s(m_DataFilePointer,"%d",&Num_buffer);
		int Seek_result=fseek(m_DataFilePointer,sizeof(char),SEEK_CUR);
		if(Seek_result)
		{
			return 0;
		}
	}
	*ElementNum=Num_buffer;
	m_EleNum=Num_buffer;
	return 1;	
}
//
//读取文件信息，包括x，y，z坐标，压力，密度，温度，网格信息
//参数：ele为网格结构体，density为密度，temperature为温度，press为压力
int CDataArrange::ReadZoneData(double* X,double* Y,double* Z,Element* Ele,double* density,double* Temperature,double* Press)
{
	int result=0;
	double Flag;
	while(!result)
	{
		result=fscanf_s(m_DataFilePointer,"%lf",&Flag);
		int seek_result=fseek(m_DataFilePointer,sizeof(char),SEEK_CUR);
		if(seek_result)
		{
			return 0;
		}
	}
	fseek(m_DataFilePointer,ftell(m_DataFilePointer)-sizeof(char),SEEK_SET);
	result=0;
	for(int i=0;i<m_NodesNum;i++)
	{
		if(!i)
		{
			*(X+i)=Flag;
			continue;
		}
		result=fscanf_s(m_DataFilePointer,"%lf",X+i);
		if(!result)
		{
			return -1;
		}

	}
	for(int i=0;i<m_NodesNum;i++)
	{
		result=fscanf_s(m_DataFilePointer,"%lf",Y+i);
		if(!result)
		{
			return -1;
		}
	}
	for(int i=0;i<m_NodesNum;i++)
	{
		result=fscanf_s(m_DataFilePointer,"%lf",Z+i);
		if(!result)
		{
			return -1;
		}
	}
	for(int i=0;i<m_NodesNum;i++)
	{
		result=fscanf_s(m_DataFilePointer,"%lf",Press+i);//压力
		if(!result)
		{
			return -1;
		}
	}
	for(int i=0;i<m_NodesNum;i++)
	{
		result=fscanf_s(m_DataFilePointer,"%lf",density+i);//密度
		if(!result)
		{
			return -1;
		}
	}
	for(int i=0;i<m_NodesNum;i++)
	{
		result=fscanf_s(m_DataFilePointer,"%lf",Temperature+i);//温度
		if(!result)
		{
			return -1;
		}
	}
	for(int i=0;i<m_EleNum;i++)
	{
		for(int j=0;j<4;j++)
		{
			result=fscanf_s(m_DataFilePointer,"%d",&((Ele+i)->face1.N1)+j);//网格第一面，包括四个点
			if(!result)
			{
				return -1;
			}
			if(result==-1)
			{
				return -2;
			}
		}
		for(int j=0;j<4;j++)
		{
			result=fscanf_s(m_DataFilePointer,"%d",&((Ele+i)->face2.N1)+j);//网格第二面，包括四个点
			if(!result)
			{
				return -1;
			}
			if(result==-1)
			{
				return -2;
			}
		}
	}
	return 1;
}
//
//生成邻接关系并存储
//ele为网格结构体
//主要生成两个数组，m_arraypointer存储每一个节点周围的八个网格信息，m_repointer存储每个网格周围六个面接网格编号及共用节点号
int CDataArrange::StoreNeighbour(Element* Ele)
{
	int* PointBuffer=(int*)malloc(sizeof(int)*9*m_NodesNum);
	for(int i=0;i<m_NodesNum;i++)
	{
		for(int j=0;j<9;j++)
		{
			if(j==0)
			{
				*(PointBuffer+i*9)=0;
			}
			else
			{
				*(PointBuffer+i*9+j)=-1;
			}
		}
	}
	for(int i=0;i<m_EleNum;i++)//按网格号遍历，遍历每一个网格的8个点，存储每个点周围的8个网格编号
	{
		Element* pos=Ele+i;
		for(int j=0;j<4;j++)//第一面的四个点
		{
			int temp=*(&(pos->face1.N1)+j);
			int count=*(PointBuffer+(temp-1)*9);
			*(PointBuffer+(temp-1)*9+count+1)=i;
			count++;
			*(PointBuffer+(temp-1)*9)=count;
		}
		for(int j=0;j<4;j++)//第二面的四个点
		{
			int temp=*(&(pos->face2.N1)+j);
			int count=*(PointBuffer+(temp-1)*9);
			*(PointBuffer+(temp-1)*9+count+1)=i;
			count++;
			*(PointBuffer+(temp-1)*9)=count;			
		}
	}
	fflush(m_ArrayPointer);
	fwrite(PointBuffer,sizeof(int),m_NodesNum*9,m_ArrayPointer);//pointbuffer存储节点周围晶格个数和编号
	free((void*)PointBuffer);
	//	extern BOOL ExitCal;//外部控制变量
	for(int m=0;m<m_EleNum;m++)//按网格号遍历，找到每个网格周围的六个面接网格并存储网格号和节点号
	{
		int UnitNum=2*6+2;//6个面接晶格，6*2，加上两个控制位，即14
		int* pRelation=(int*)malloc(sizeof(int)*UnitNum);//prelation/m_arraypointer存储晶格周围晶格编号和共用节点号
		for(int i=2;i<UnitNum;i++)
		{
			*(pRelation+i)=-1;
		}
		*(pRelation+1)=0;
		*pRelation=m;
		//		if(ExitCal)
		//		{
		//			break;
		//		}

		int numcount=0;
		for(int r=0;r<5;r++)//r为当前晶格的顶点编号，只需找0,1,2,4这四个点即可完整遍历周围的6个面接晶格
		{
			if(r==3)
				continue;
			int yuzhi;//阈值，用于控制每个顶点最多寻找到的面接晶格数，0号为三个，其他均为一个，3+1+1+1=6
			if(r==0)
				yuzhi=3;
			else
				yuzhi=1;
			int point1=0,point2=0,temp;//用于寻找共用面的三个点，其中temp为记录遍历点，其他两个为辅助遍历点

			Element* pos=Ele+m;



			for(int e=0;e<yuzhi;e++)
			{
				if(r==0)
				{
					if(e==0)
					{
						temp=*(&(pos->face1.N1)+0);//左
						point1=*(&(pos->face1.N1)+3);
						point2=*(&(pos->face2.N1)+0);
					}
					else if(e==1)
					{
						temp=*(&(pos->face1.N1)+0);
						point1=*(&(pos->face1.N1)+3);//上
						point2=*(&(pos->face1.N1)+1);
					}
					else 
					{
						temp=*(&(pos->face1.N1)+0);
						point1=*(&(pos->face1.N1)+1);//后
						point2=*(&(pos->face2.N1)+0);
					}
				}
				else if(r==1)
				{
					temp=*(&(pos->face1.N1)+1);//右
					point1=*(&(pos->face1.N1)+2);
					point2=*(&(pos->face2.N1)+1);
				}else if(r==2)
				{
					temp=*(&(pos->face1.N1)+2);//前
					point1=*(&(pos->face1.N1)+3);
					point2=*(&(pos->face2.N1)+3);
				}
				else if(r==4)
				{
					temp=*(&(pos->face2.N1)+0);//下
					point1=*(&(pos->face2.N1)+1);
					point2=*(&(pos->face2.N1)+2);
				}

				fseek(m_ArrayPointer,sizeof(int)*(temp-1)*9,SEEK_SET);
				int buffer[9];
				fread(buffer,sizeof(int),9,m_ArrayPointer);
				int count=buffer[0];

				for(int k=0;k<count;k++)
				{
					Element* temp_ele=Ele+buffer[k+1];
					bool found=false;
					if(*(&(temp_ele->face1.N1)+0)==point1||*(&(temp_ele->face1.N1)+1)==point1||*(&(temp_ele->face1.N1)+2)==point1||*(&(temp_ele->face1.N1)+3)==point1
						||*(&(temp_ele->face2.N1)+0)==point1||*(&(temp_ele->face2.N1)+1)==point1||*(&(temp_ele->face2.N1)+2)==point1||*(&(temp_ele->face2.N1)+3)==point1)
					{

						if(*(&(temp_ele->face1.N1)+0)==point2||*(&(temp_ele->face1.N1)+1)==point2||*(&(temp_ele->face1.N1)+2)==point2||*(&(temp_ele->face1.N1)+3)==point2
							||*(&(temp_ele->face2.N1)+0)==point2||*(&(temp_ele->face2.N1)+1)==point2||*(&(temp_ele->face2.N1)+2)==point2||*(&(temp_ele->face2.N1)+3)==point2)
						{
							if(buffer[k+1]!=m)
							{
								*(pRelation+2+numcount*2)=buffer[k+1];
								*(pRelation+2+numcount*2+1)=temp;
								found=true;
								numcount++;
*(pRelation+1)=numcount;
							}

						}

					}
					if(found)
						break;


				}
			}


		}

		/*bool* pFlag=(bool*)malloc(sizeof(bool)*m_EleNum);
		for(int f=0;f<m_EleNum;f++)
		{
		*(pFlag+f)=false;
		}
		Element* pos=Ele+m;
		bool Done=false;
		for(int up=0;up<4;up++)
		{
		int temp=*(&(pos->face1.N1)+up);
		fseek(m_ArrayPointer,sizeof(int)*(temp-1)*9,SEEK_SET);
		int buffer[9];
		fread(buffer,sizeof(int),9,m_ArrayPointer);
		int count=buffer[0];
		for(int vertex=0;vertex<count;vertex++)
		{
		if(buffer[vertex+1]==-1)
		{
		break;
		}
		if(!*(pFlag+buffer[vertex+1]))
		{
		*(pFlag+buffer[vertex+1])=1;
		if(buffer[vertex+1]!=m)
		{
		int temp_count=*(pRelation+1);
		*(pRelation+2+temp_count*2)=buffer[vertex+1];
		*(pRelation+2+temp_count*2+1)=temp;
		temp_count++;
		*(pRelation+1)=temp_count;		
		if(temp_count==26)
		{
		Done=true;
		}
		}
		}
		if(Done)
		{
		break;
		}
		}
		if(Done)
		break;

		}
		if(Done)
		{
		continue;
		}
		for(int down=0;down<4;down++)
		{
		int temp=*(&(pos->face2.N1)+down);
		fseek(m_ArrayPointer,sizeof(int)*(temp-1)*9,SEEK_SET);
		int buffer[9];
		fread(buffer,sizeof(int),9,m_ArrayPointer);
		int count=buffer[0];
		for(int vertex=0;vertex<count;vertex++)
		{
		if(buffer[vertex+1]==-1)
		{
		break;
		}
		if(!*(pFlag+buffer[vertex+1]))
		{
		*(pFlag+buffer[vertex+1])=1;
		if(buffer[vertex+1]!=m)
		{
		int temp_count=*(pRelation+1);
		*(pRelation+2+temp_count*2)=buffer[vertex+1];
		*(pRelation+2+temp_count*2+1)=temp;
		temp_count++;
		*(pRelation+1)=temp_count;		
		if(temp_count==26)
		{
		Done=true;
		}
		}
		}
		if(Done)
		{
		break;
		}
		}
		if(Done)
		break;		
		}
		free((void*)pFlag);*/
		fflush(m_RePointer);
		fwrite(pRelation,sizeof(int),UnitNum,m_RePointer);
		fflush(m_RePointer);
/*FILE* file;
fopen_s(&file,"H:\\pjs\\test\\无标题_流场光程差\\zj.txt","a");
for(int p=0;p<7;p++)
fprintf_s(file,"%d\n",*(pRelation+p*2));
fclose(file);*/                /////////////////////用于测试/////////////////////////////////
	}

	//	if(ExitCal)
	//	{
	//		return 0;
	//	}
	return 1;
}
//
//寻找流场表面的网格
//参数：ptoplayer为外表面数组，pbottomlayer为内表面数组，ele为网格结构体
//寻找方式：当一个网格周围没有六个面接网格时，就认为它是表面上的网格，再调用getupface函数找出该网格上下延伸后所对应的上下表面
//然后将找到的上下表面依次存在两个数组中
int CDataArrange::FindSurface(TopLayer** pTopLayer,TopLayer** pBottomLayer,Element* Ele,double* X,double* Y,double* Z)
{
	bool* pFlag=(bool*)malloc(sizeof(bool)*m_EleNum);//pflag作用是标识上下表面上的某个表面是否存储过
	for(int m=0;m<m_EleNum;m++)
	{
		*(pFlag+m)=false;
	}
	int LayerNum=0;
	int count=0;
	for(int i=0;i<m_EleNum;i++)
	{
		int UnitNum=2*6+2;
		int* pRelation=(int*)malloc(sizeof(int)*UnitNum);
		fseek(m_RePointer,sizeof(int)*i*UnitNum,SEEK_SET);
		fread(pRelation,sizeof(int),UnitNum,m_RePointer);
		int sideNum=*(pRelation+1);
		if(sideNum!=6)//当周围面接网格数不是6的时候
		{
			TopLayer temp_Layer;
			TopLayer temp_Layer_1;
			GetUpFace(i,&temp_Layer,&temp_Layer_1,&LayerNum,Ele,X,Y,Z);//找到该表面网格上下方向所对应的内外表面
			if(!*(pFlag+temp_Layer.Index))//将内外表面分别存储在ptoplayer和pbottomlayer中
			{
				AddTopElement(&temp_Layer,count,pTopLayer);
				AddTopElement(&temp_Layer_1,count,pBottomLayer);
				*(pFlag+temp_Layer.Index)=1;
				count++;
			}
		}
		free((void*)pRelation);
	}
	free((void*)pFlag);
	return count;
}
//
//找到一个网格上下方向对应的内外表面，相对于流场圆心而言
//参数：index是网格号索引，player、player1用来存放两个表面，ele为网格数组，layernum参数没有用处，可能是测试用
//寻找方法：先找到该网格的三个面对，然后通过面对向三个方向延伸，计算延伸后的面对到原点的距离差，从而
//筛选出上下方向，从而找到该网格上下方向对应的两个外表面，再计算两个面到原点距离，从而最终找出内、外表面
int CDataArrange::GetUpFace(int Index,TopLayer* pLayer,TopLayer* pLayer1,int* LayerNum,Element* Ele,double* X,double* Y,double* Z)
{
	Up_Down Pair[3];//用于存储一个网格的六个面，即三个面对
	int count[3];
	for(int i=0;i<3;i++)
	{
		Pair[i].Index_1=Index;
		Pair[i].Index_2=Index;
	}
	///////////////////////////////////////////////////////////
	FaceEqual(&((Ele+Index)->face1),&(Pair[0].face_1));//原网格第一个面对，
	FaceEqual(&((Ele+Index)->face2),&(Pair[0].face_2));
	Pair[0].Direction_1=1;
	Pair[0].Direction_2=1;
	Surface buffer;
	buffer.N1=((Ele+Index))->face1.N1;//原网格第二个面对，
	buffer.N2=((Ele+Index))->face1.N2;
	buffer.N3=((Ele+Index))->face2.N2;
	buffer.N4=((Ele+Index))->face2.N1;
	FaceEqual(&buffer,&(Pair[1].face_1));
	buffer.N1=((Ele+Index))->face1.N4;
	buffer.N2=((Ele+Index))->face1.N3;
	buffer.N3=((Ele+Index))->face2.N3;
	buffer.N4=((Ele+Index))->face2.N4;
	FaceEqual(&buffer,&(Pair[1].face_2));
	Pair[1].Direction_1=2;
	Pair[1].Direction_2=2;
	buffer.N1=((Ele+Index))->face1.N1;//原网格第三个面对，
	buffer.N2=((Ele+Index))->face1.N4;
	buffer.N3=((Ele+Index))->face2.N4;
	buffer.N4=((Ele+Index))->face2.N1;
	FaceEqual(&buffer,&(Pair[2].face_1));
	buffer.N1=((Ele+Index))->face1.N2;
	buffer.N2=((Ele+Index))->face1.N3;
	buffer.N3=((Ele+Index))->face2.N3;
	buffer.N4=((Ele+Index))->face2.N2;
	FaceEqual(&buffer,&(Pair[2].face_2));
	Pair[2].Direction_1=3;
	Pair[2].Direction_2=3;
	for(int i=0;i<3;i++)
	{
		count[i]=Analysis2(Pair+i,Ele);//此循环找到三个方向延伸后的面对
	}
	double disArray[3];
	for(int i=0;i<3;i++)
	{
		int temp1=Pair[i].Index_1;
		int temp2=Pair[i].Index_2;
		double tempdis1=SolveAbsDistance((Ele+temp1)->face1.N1,X,Y,Z);
		double tempdis2=SolveAbsDistance((Ele+temp2)->face1.N1,X,Y,Z);//此循环计算三个面对到原点的距离差
		double cha=tempdis1-tempdis2;
		if(cha<0)
		{
			cha=-1*cha;
		}
		disArray[i]=cha;
	}
	double BaseAxis=disArray[0];
	int BaseAxisIndex=0;
	for(int i=0;i<3;i++)
	{
		if(disArray[i]>BaseAxis)
		{
			BaseAxis=disArray[i];//此循环筛选出上下面对，因为上下面对到原点的距离差最大
			BaseAxisIndex=i;
		}
	}
	*LayerNum=count[BaseAxisIndex]+1;
	int Index1=Pair[BaseAxisIndex].Index_1;
	int Index2=Pair[BaseAxisIndex].Index_2;
	double Dis1=SolveAbsDistance((Ele+Index1)->face1.N1,X,Y,Z);
	double Dis2=SolveAbsDistance((Ele+Index2)->face1.N1,X,Y,Z);
	if(Dis1>Dis2)//dis1>dis2，则说明第一面是上表面，因为上表面到原点距离比下表面大
	{
		FaceEqual(&(Pair[BaseAxisIndex].face_1),&(pLayer->face));
		FaceEqual(&(Pair[BaseAxisIndex].face_2),&(pLayer1->face));
		pLayer->direction=Pair[BaseAxisIndex].Direction_1;
		pLayer1->direction=Pair[BaseAxisIndex].Direction_2;
		pLayer->Index=Pair[BaseAxisIndex].Index_1;
		pLayer1->Index=Pair[BaseAxisIndex].Index_2;
		return Pair[BaseAxisIndex].Index_1;
	}
	else
	{
		FaceEqual(&(Pair[BaseAxisIndex].face_2),&(pLayer->face));
		FaceEqual(&(Pair[BaseAxisIndex].face_1),&(pLayer1->face));
		pLayer->direction=Pair[BaseAxisIndex].Direction_2;
		pLayer1->direction=Pair[BaseAxisIndex].Direction_1;
		pLayer->Index=Pair[BaseAxisIndex].Index_2;
		pLayer1->Index=Pair[BaseAxisIndex].Index_1;
		return Pair[BaseAxisIndex].Index_2;
	}
}
//
//赋值一个面
//origin为赋值源，des为需要赋值的面
void CDataArrange::FaceEqual(Surface* Origin,Surface* Des)
{
	Des->N1=Origin->N1;
	Des->N2=Origin->N2;
	Des->N3=Origin->N3;
	Des->N4=Origin->N4;	
}

//
//找出一个面对延伸后的面对，比如上下面对，找到的就是该网格向上或向下所对应的外表面
//参数：pupdown是一个面对，ele是网格数组
//此函数中编号1/2，是区分一个面对的两个表面
int CDataArrange::Analysis2(Up_Down* pUpDown,Element* Ele)
{
	int ElementCount=0;
	int FindIndex1=pUpDown->Index_1;
	int FindIndex2=pUpDown->Index_2;
	int Dir1=pUpDown->Direction_1;
	int Dir2=pUpDown->Direction_2;
	Surface* pTemp=(Surface*)malloc(6*sizeof(Surface));
	Surface face1;
	Surface face2;
	FaceEqual(&(pUpDown->face_1),&face1);
	FaceEqual(&(pUpDown->face_2),&face2);
	bool bDone=false;
	bool bErrorPointer=false;
	bool bDone1=false;
	bool bDone2=false;
	///////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////
	while(!bDone)
	{
		bool bFind1=false;
		bool bFind2=false;
		bool bCircled1=false;
		bool bCircled2=false;
		int UnitNum=2*6+2;
		int* pRelation1=(int*)malloc(sizeof(int)*UnitNum);
		int* pRelation2=(int*)malloc(sizeof(int)*UnitNum);
		fseek(m_RePointer,sizeof(int)*FindIndex1*UnitNum,SEEK_SET);
		fread(pRelation1,sizeof(int),UnitNum,m_RePointer);
		fseek(m_RePointer,sizeof(int)*FindIndex2*UnitNum,SEEK_SET);
		fread(pRelation2,sizeof(int),UnitNum,m_RePointer);
		int sideNum1=*(pRelation1+1);
		int sideNum2=*(pRelation2+1);
		if(!bDone1)
		{
			for(int side=0;side<sideNum1;side++)//在邻接网格中遍历
			{
				int Direction1=*(pRelation1+2+2*side+1);
				if((Direction1!=face1.N1)&&(Direction1!=face1.N2)&&(Direction1!=face1.N3)&&(Direction1!=face1.N4))
				{
					continue;
				}
				int Index1=*(pRelation1+2+2*side);
				FaceArrange(Ele+Index1,pTemp);//生成相应网格的6个面，存在ptemp中
				for(int i=0;i<6;i++)//在当前遍历的网格的六个面中遍历，找到共用面，
				{
					if(CompareFace(pTemp+i,&face1))
					{
						bFind1=true;
						if(Index1==FindIndex2)
						{
							bCircled1=true;
							break;
						}
						FindIndex1=Index1;
						if(i%2)
						{
							FaceEqual(pTemp+i-1,&face1);//这里赋值共用面的对应面，作用是向两个方向延伸
						
						}
						else
						{
							FaceEqual(pTemp+i+1,&face1);
						}
						Dir1=i/2+1;
						ElementCount++;
						break;
					}
				}
				if(bFind1||bCircled1)
				{
					break;
				}
			}
			if((!bFind1)||bCircled1)
			{
				bDone1=true;
			}
		}
		if(!bDone2)
		{
			for(int side=0;side<sideNum2;side++)
			{
				int Direction2=*(pRelation2+2+2*side+1);
				if((Direction2!=face2.N1)&&(Direction2!=face2.N2)&&(Direction2!=face2.N3)&&(Direction2!=face2.N4))
				{
					continue;
				}
				int Index2=*(pRelation2+2+2*side);
				FaceArrange(Ele+Index2,pTemp);
				for(int i=0;i<6;i++)
				{
					if(CompareFace(pTemp+i,&face2))
					{
						bFind2=true;
						if(Index2==FindIndex1)
						{
							bCircled2=true;
							break;
						}
						FindIndex2=Index2;
						if(i%2)
						{
							FaceEqual(pTemp+i-1,&face2);
						}
						else
						{
							FaceEqual(pTemp+i+1,&face2);
						}
						Dir2=i/2+1;
						ElementCount++;
						break;
					}
				}
				if(bFind2||bCircled2)
				{
					break;
				}				
			}
			if((!bFind2)||bCircled2)
			{
				bDone2=true;
			}			
		}
		if(bDone1&&bDone2)
		{
			bDone=true;
		}
		free((void*)pRelation1);
		free((void*)pRelation2);
	}
	/////////////////////////////////////////////////////////////////////
	free((void*)pTemp);
	///////////////////////////////////////////////////////////////////////
	FaceEqual(&face1,&(pUpDown->face_1));
	FaceEqual(&face2,&(pUpDown->face_2));
	pUpDown->Direction_1=Dir1;
	pUpDown->Direction_2=Dir2;
	pUpDown->Index_1=FindIndex1;
	pUpDown->Index_2=FindIndex2;
	return ElementCount;
	return 0;
}
//
//将一个网格的六个面整理出来并存储
//参数：pelement为待整理的网格，ptemp用来存储六个面
void CDataArrange::FaceArrange(Element* pElement,Surface* pTemp)
{
	Surface buffer;
	FaceEqual(&(pElement->face1),pTemp);//
	FaceEqual(&(pElement->face2),pTemp+1);//
	buffer.N1=pElement->face1.N1;
	buffer.N2=pElement->face1.N2;
	buffer.N3=pElement->face2.N2;
	buffer.N4=pElement->face2.N1;
	FaceEqual(&buffer,pTemp+2);//
	buffer.N1=pElement->face1.N4;
	buffer.N2=pElement->face1.N3;
	buffer.N3=pElement->face2.N3;
	buffer.N4=pElement->face2.N4;
	FaceEqual(&buffer,pTemp+3);//
	buffer.N1=pElement->face1.N1;
	buffer.N2=pElement->face1.N4;
	buffer.N3=pElement->face2.N4;
	buffer.N4=pElement->face2.N1;
	FaceEqual(&buffer,pTemp+4);//
	buffer.N1=pElement->face1.N2;
	buffer.N2=pElement->face1.N3;
	buffer.N3=pElement->face2.N3;
	buffer.N4=pElement->face2.N2;
	FaceEqual(&buffer,pTemp+5);//
}
//
//比较两个面是否是同一面
//参数：face1，face2为待比较的两个面
int CDataArrange::CompareFace(Surface* face1, Surface* face2)
{
	int times=0;
	for(int i=0;i<4;i++)
	{
		int temp=*(&(face1->N1)+i);
		for(int j=0;j<4;j++)
		{
			if(temp==*(&(face2->N1)+j))
			{
				times++;
			}
			if(times==3)
			{
				return 1;
			}
		}
	}
	return 0;
}
//
//计算一个网格第一面的第一个点离原点的距离
//参数：index为网格索引，x，y，z为三个坐标数组
double CDataArrange::SolveAbsDistance(int Index,double* X,double* Y,double* Z)
{
	double x_1=*(X+Index-1);
	double y_1=*(Y+Index-1);
	double z_1=*(Z+Index-1);
	return sqrt((x_1-0.0)*(x_1-0.0)+(y_1-0.0)*(y_1-0.0)+(z_1-0.0)*(z_1-0.0));
}
//
//将找到的面加到相应的数组中
//参数：player是找到的某个面，ptoppayer是存储面的数组，可以取值为上表面数组和下表面数组，num标识数组中现有面的个数
int CDataArrange::AddTopElement(TopLayer* pLayer,int Num,TopLayer** pTopLayer)
{
	TopLayer* temp=(TopLayer*)realloc((void*)(*pTopLayer),sizeof(TopLayer)*(Num+1));
	if(temp)
	{

		(temp+Num)->Index=pLayer->Index;
		FaceEqual(&(pLayer->face),&((temp+Num)->face));	
		(temp+Num)->direction=pLayer->direction;
		if(temp!=*(pTopLayer))
		{
			*(pTopLayer)=temp;	
		}
		return 1;
	}
	else
	{
		return 0;
	}
}
/*
int CDataArrange::WriteVertex(int* pVertex, double* X , double* Y , double* Z , double* Tem,Element* pEle,char* path)
{
CArray <int,int&> m_PointArray;
CArray <int,int&> m_BrickArray;
//	Connection* pConnect=Vertex->Head;
int Index=*pVertex;//Vertex->Index;
Element* pElement=pEle+Index;
UINT count=*(pVertex+1);
m_PointArray.Add(pElement->face1.N1);
m_PointArray.Add(pElement->face1.N2);
m_PointArray.Add(pElement->face1.N3);
m_PointArray.Add(pElement->face1.N4);
m_PointArray.Add(pElement->face2.N1);
m_PointArray.Add(pElement->face2.N2);
m_PointArray.Add(pElement->face2.N3);
m_PointArray.Add(pElement->face2.N4);
///////////////////////////////////////////////
Index=1;
m_BrickArray.Add(Index);
Index=2;
m_BrickArray.Add(Index);
Index=3;
m_BrickArray.Add(Index);
Index=4;
m_BrickArray.Add(Index);
Index=5;
m_BrickArray.Add(Index);
Index=6;
m_BrickArray.Add(Index);
Index=7;
m_BrickArray.Add(Index);
Index=8;
m_BrickArray.Add(Index);
for(int i=0;i<count;i++)
{
Index=*(pVertex+2+i*2);
pElement=pEle+Index;
for(int i=0;i<4;i++)
{
int temp=*(&(pElement->face1.N1)+i);
bool bHave=false;
for(int j=0;j<m_PointArray.GetCount();j++)
{
if(temp==m_PointArray.GetAt(j))
{
bHave=true;
int buffer=j+1;
m_BrickArray.Add(buffer);
break;
}
}
if(!bHave)
{
m_PointArray.Add(temp);
int buffer=m_PointArray.GetCount();
m_BrickArray.Add(buffer);
}
}
for(int i=0;i<4;i++)
{
int temp=*(&(pElement->face2.N1)+i);
bool bHave=false;
for(int j=0;j<m_PointArray.GetCount();j++)
{
if(temp==m_PointArray.GetAt(j))
{
bHave=true;
int buffer=j+1;
m_BrickArray.Add(buffer);
break;
}
}
if(!bHave)
{
m_PointArray.Add(temp);
int buffer=m_PointArray.GetCount();
m_BrickArray.Add(buffer);
}
}		
}
CStringA Header;
CStringA ZoneData="";
CStringA PointData="";
FILE* pStream=NULL;
fopen_s(&pStream,path,"w+");
CStdioFile FeFile(pStream);
if(!FeFile.m_pStream)
{
::AfxMessageBox(_T("创建文件失败"));
//		return 0;
}
Header.Format("TITLE = \"abc\"\r\n\
VARIABLES = \"X\", \"Y\", \"Z\", \"Temperature\"\r\n\
ZONE NODES=%d, ELEMENTS=%d, DATAPACKING=POINT, ZONETYPE=FEBRICK\r\n",m_PointArray.GetCount(),m_BrickArray.GetCount()/8);
for(int i=0;i<m_BrickArray.GetCount()/8;i++)
{
CStringA temp="";
for(int j=i*8;j<8*i+8;j++)
{
CStringA temp_1;
temp_1.Format("%d ",m_BrickArray.GetAt(j));
temp=temp+temp_1;
}
temp=temp+"\r\n";
ZoneData=ZoneData+temp;
}
for(int i=0;i<m_PointArray.GetCount();i++)
{
CStringA temp="";
temp.Format("%.10f %.10f %.10f %.10f\r\n",*(X+m_PointArray.GetAt(i)-1),*(Y+m_PointArray.GetAt(i)-1),*(Z+m_PointArray.GetAt(i)-1),*(Tem+m_PointArray.GetAt(i)-1));
PointData=PointData+temp;
}
FeFile.Write((LPCSTR)Header,Header.GetLength());
FeFile.Write((LPCSTR)PointData,PointData.GetLength());
FeFile.Write((LPCSTR)ZoneData,ZoneData.GetLength());
FeFile.Close();
return 1;
}

int CDataArrange::WriteTopLayer(struct TopLayer* pTopLayer, int ElementCount,double *Xcontainer,double *Ycontainer,double *Zcontainer,double *Value,char* path)
{
FILE *fp;
//	char path[100]={"C:\\Users\\Admin\\Desktop\\Surface_pp1.dat"};
if((fopen_s(&fp,path,"w"))!=0)
{
//	printf("表面文件无法写！");
//	getchar();
//	exit(0);
//		::AfxMessageBox(_T("Writer_Failed!"));
return -1;
}


int* PointArray=NULL;
int* IndexArray=NULL;
int PointCount=0;
int IndexCount=0;
for(int i=0;i<ElementCount;i++)
{
for(int j=0;j<4;j++)
{
int temp_Index=*(&((pTopLayer+i)->face.N1)+j);
bool bAdded=false;
int SameIndex=0;
for(int k=0;k<PointCount;k++)
{
if(temp_Index==*(PointArray+k))
{
bAdded=true;
SameIndex=k;
break;
}
}
if(!bAdded)
{
int* tempPointer=(int*)realloc((void*)PointArray,(PointCount+1)*sizeof(int));
*(tempPointer+PointCount)=temp_Index;
PointCount++;
if(tempPointer!=PointArray)
{
PointArray=tempPointer;
}
int temp=PointCount;
int* tempPointer_1=(int*)realloc((void*)IndexArray,(IndexCount+1)*sizeof(int));
*(tempPointer_1+IndexCount)=temp;
IndexCount++;
if(tempPointer_1!=IndexArray)
{
IndexArray=tempPointer_1;
}
}
else
{
int temp=SameIndex+1;
int* tempPointer=(int*)realloc((void*)IndexArray,(IndexCount+1)*sizeof(int));
*(tempPointer+IndexCount)=temp;
IndexCount++;
if(tempPointer!=IndexArray)
{
IndexArray=tempPointer;
}
}
}
}
//////////////////////////////////////////////////////////////////////////////////////////
fprintf_s(fp,"TITLE = \"abc\"\r\nVARIABLES = \"X\", \"Y\", \"Z\", \"Temperature\"\r\nZONE NODES=%d, ELEMENTS=%d, DATAPACKING=POINT, ZONETYPE=FEQUADRILATERAL\r\n",PointCount,IndexCount/4);
for(int i=0;i<PointCount;i++) //PointCount表层元素的个数
{
int Index=*(PointArray+i);
fprintf_s(fp,"%.10f %.10f %.10f %.10f\r\n",*(Xcontainer+Index-1),*(Ycontainer+Index-1),*(Zcontainer+Index-1),*(Value+Index-1));
}
for(int i=0;i<IndexCount/4;i++)
{
for(int j=0;j<4;j++)
{
fprintf_s(fp,"%d ",*(IndexArray+4*i+j));
}
fprintf_s(fp,"\n");
}

fclose(fp);
free(PointArray);
free(IndexArray);
return 1;
}

int CDataArrange::WriteArrange(int up, int down,double* X , double* Y , double* Z , double* Tem,Element* pEle)
{
for(int i=up;i<down+1;i++)
{
CString File;
File.Format(_T("C:\\Users\\Admin\\Desktop\\File%d.dat"),i);
int UnitNum=2*26+2;
int* pRelation=(int*)malloc(sizeof(int)*UnitNum);
fseek(m_RePointer,sizeof(int)*i*UnitNum,SEEK_SET);
fread(pRelation,sizeof(int),UnitNum,m_RePointer);	
WriteVertex(pRelation,X,Y,Z,Tem,pEle,((CStringA)File).GetBuffer());
free((void*)pRelation);
}
return 1;
}
*/


//
//获取一个网格周围的邻接网格，这里结构改动后的邻接网格指的是六个面接网格
//参数：gridnum为该网格的编号，pre是准备存放邻接网格信息的数组
int CDataArrange::GetNeighbour(int GridNum,int* pRe)
{
	int UnitNum=2*6+2;
	int* pRelation=(int*)malloc(sizeof(int)*UnitNum);
	fseek(m_RePointer,sizeof(int)*GridNum*UnitNum,SEEK_SET);//m_repointer是内存中保存邻接关系的数组，在storeneighbor函数中生成
	fread(pRelation,sizeof(int),UnitNum,m_RePointer);
	int count=*(pRelation+1);
	for(int i=0;i<6;i++)
	{
		*(pRe+i)=*(pRelation+2+2*i);
	}
	free((void*)pRelation);
	return count;
}
//
//找出非轴向的四个面接网格，比如轴向是1，代表的是上下网格方向，那么找到的就是左右前后四个网格
//参数：index为当前中心网格，ele为网格数组，neighberarray为用来存储找到网格的数组，baseaxis为轴向
void CDataArrange::FindNeighbour(int Index,Element* Ele,int* NeighbourArray,int BaseAxis)///NeighbourArray size为4的int型数组，初始化为-1，存储相邻Element序号，
{
	Surface face[6];
	FaceArrange((Ele+Index),face);
	//	Connection* pConnection=(pVertex+Index)->Head;
	int UnitNum=2*6+2;
	int* pRelation=(int*)malloc(sizeof(int)*UnitNum);
	fseek(m_RePointer,sizeof(int)*Index*UnitNum,SEEK_SET);
	fread(pRelation,sizeof(int),UnitNum,m_RePointer);
	int count=*(pRelation+1);
	Surface FaceCompare[4];
	switch (BaseAxis)//根据轴向决定是哪四个共用面
	{
	case 1:
		{
			FaceEqual(face+2,FaceCompare);
			FaceEqual(face+3,FaceCompare+1);
			FaceEqual(face+4,FaceCompare+2);
			FaceEqual(face+5,FaceCompare+3);
		}
		break;
	case 2:
		{
			FaceEqual(face,FaceCompare);
			FaceEqual(face+1,FaceCompare+1);
			FaceEqual(face+4,FaceCompare+2);
			FaceEqual(face+5,FaceCompare+3);
		}
		break;
	case 3:
		{
			FaceEqual(face,FaceCompare);
			FaceEqual(face+1,FaceCompare+1);
			FaceEqual(face+2,FaceCompare+2);
			FaceEqual(face+3,FaceCompare+3);
		}
		break;
	}
	int num=0;
	for(int i=0;i<count;i++)//在邻接网格中遍历，一个个找出共用面对应的面接网格
	{
		int temp=*(pRelation+2+2*i);
		int NeighbourPoint=*(pRelation+2+2*i+1);
		bool find=false;
		for(int i=0;i<4;i++)
		{
			if((NeighbourPoint!=FaceCompare[i].N1)&&(NeighbourPoint!=FaceCompare[i].N2)&&(NeighbourPoint!=FaceCompare[i].N3)&&(NeighbourPoint!=FaceCompare[i].N4))
			{
				continue;
			}
			else
			{
				Surface tempFace[6];
				FaceArrange((Ele+temp),tempFace);
				for(int j=0;j<6;j++)
				{
					if(CompareFace(tempFace+j,FaceCompare+i))
					{
						*(NeighbourArray+num)=temp;
						num++;
						find=true;
						break;
					}
				}
			}
			if(find)
			{
				break;
			}
		}
		if(num==4)
		{
			break;
		}
	}
	free((void*)pRelation);
}