#pragma once
#include "Projection.h"

void excludeCoveredUnits(ImagePlane &plane, vector<Node> &nodes, vector<Unit> &units);


void deleteCovered(ImagePlane &plane, ImageUnit &imageUnit, vector<Node> &nodes, vector<Unit> &units);
int isCovered(Unit &unit, ImagePlane &plane, vector<Node> &nodes, vector<Unit> &units);
int isClosest(Node &point, int unitIndex, ImagePlane &plane, vector<Node> &nodes, vector<Unit> &units);
void calcPlane(double &a, double &b, double &c, double &d, int* corners, vector<Node> nodes);
int findMinDist(vector<double> &distance, vector<int> &indexOfDistance);