#include "Grid.h"
#include "Projection.h"
#include "Radiation.h"



int main()
{
    //=========================
    // 读取文件
    double L;   // 飞行器与卫星之间的距�?
    double viewDegree;  // 相机视场�?
    double d0;     // 表面网格单元最小尺�?
    vector<Node> originNodes;
    vector<Unit> units;
    double l1, l2;  // 波长
    //=========================
    
    double eulerAngle[3];   // 运载体坐标系欧拉�?
    vector<Node> nodes;     // 摄像机坐标系下的网格�?
    transformCoorSys(originNodes, units, L);
    ImagePlane plane = ImagePlane(viewDegree, L, d0);
    for (int i = 0; i < units.size(); i++)
    {
        projectUnit(units[i], plane, nodes, units);
    }
    
    excludeCoveredUnits(plane, nodes, units);
    double radiation = calcTotalRadiation(plane, nodes, units, l1, l2, L);
}

