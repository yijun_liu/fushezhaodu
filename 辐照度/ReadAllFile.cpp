#include "stdafx.h"
#include <iostream>
#include <stdlib.h>
#include <malloc.h>
#include <cmath>
using namespace std;
#include"FlowOPD.h"

/************************************************************************/
/*
输入：读取数据的路径-char*ReadPath
输出：无
function:读取Tecplot格式的文件，对坐标点、密度、压力、温度、连接关系等信息
																		*/
/************************************************************************/
void FlowOPD::Read_All_Data(CString ReadPath) //读取数据并作相应的处理
{
	int temp;  //下面各个函数的返回值，无实际意义。
CStringA Path1=(CStringA)ReadPath;
 char *readpath=Path1.GetBuffer(Path1.GetLength());
	temp=DATA.SetDataPath(readpath);

	switch(temp) //检查文件路径是否正确
	{
	case -2:cout<<"空字符串！";exit (0);break;
	case -1:cout<<"打开文件失败！";exit (0);break;
	default:break;
	}

	temp=DATA.ReadZoneHead(&NodesNum,&ElementsNum);  //读取文件头信息
	if(temp==0)
	{
		cout<<"读取头文件失败！";
		exit (0);
	}

	Xcontainer=(double*)malloc(NodesNum*sizeof(double)); //坐标  
	Ycontainer=(double*)malloc(NodesNum*sizeof(double));
	Zcontainer=(double*)malloc(NodesNum*sizeof(double));
	Density=(double*)malloc(NodesNum*sizeof(double));  //密度
	Press=(double*)malloc(NodesNum*sizeof(double));  //压力
	Temperature=(double*)malloc(NodesNum*sizeof(double));  //温度
	ElementContainer=(struct Element*)malloc(ElementsNum*sizeof(struct Element)); //六面体

	temp=DATA.ReadZoneData(Xcontainer,Ycontainer,Zcontainer,ElementContainer,Density,Temperature,Press); //读取坐标点信息以及温度、压力、密度信息
	temp=DATA.StoreNeighbour(ElementContainer);  //读取并存储连接关系==Tecplot格式
	count_surface=DATA.FindSurface(&pTopLayer,&pBottomLayer,ElementContainer,Xcontainer,Ycontainer,Zcontainer);  //寻找流场内外表面，表面点数目返回count_surface
	DataArr(Temperature,Density,Press,NodesNum);  //整理排列密度、温度、压力的数据，使名称与数据相对应
	refractiveIndex_min=DensityToRefrac(Density,NodesNum); //将密度转化成折射率，转化后仍存储在Density数组中----返回的是所有折射率中的最小值
}


/************************************************************************/
/*
输入：依次为读取关于流场Tecplot文件的信息double *Temperature,double *Density,double *Press,int NodesNum
输出：无
function:使名称与数据相对应
																		*/
/************************************************************************/
void FlowOPD::DataArr(double *Temperature,double *Density,double *Press,int NodesNum)
{
	double arr[3]={0,0,0}; //存储压力、密度、温度三个矩阵的最大值，排序
	for(int i=0;i<NodesNum;i++)//密度
	{
		if(arr[0]<*(Density+i))
			arr[0]=*(Density+i);
	}
	for(int i=0;i<NodesNum;i++)  //压力
	{
		if(arr[1]<*(Press+i))
			arr[1]=*(Press+i);
	}
	for(int i=0;i<NodesNum;i++)  //温度
	{				
		if(arr[2]<*(Temperature+i))
			arr[2]=*(Temperature+i);
	}
	double *point[3];
	point[0]=Density;
	point[1]=Press;
	point[2]=Temperature;
	double max;
	double min;
	int kmax;
	int kmin;
	int k;
	kmax=0;
	kmin=0;
	max=arr[0];
	min=arr[0];
	for(int j=1;j<3;j++)
	{
		if(max<arr[j])
		{
			max=arr[j];
			kmax=j;
		}
		if(min>arr[j])
		{
			min=arr[j];
			kmin=j;
		}
	}
	for(int j=0;j<3;j++)
	{
		if((j!=kmin)&&(j!=kmax))
		{
			k=j;
		}
	}

	Press=point[kmax];
	Density=point[kmin];
	Temperature=point[k];
}

/************************************************************************/
/*
输入：流场的密度-double *Density,节点的个数-double NodesNum
输出：折射率密度的最小值
function:将密度转化成折射率
																		*/
/************************************************************************/
double FlowOPD::DensityToRefrac(double *Density,double NodesNum)
{
	double Refracmin;
	double KGD;       
	double lamada;
		lamada=(lamada1+lamada2)/2;

	KGD=(2.23e-1)*(1+(7.52e-3)/(pow(lamada,2)));
	for(int i=0;i<NodesNum;i++)
	{
		*(Density+i)=(*(Density+i))*KGD*0.001+1;  //将密度准化成折射率
        if(i==0)
			Refracmin=*(Density+i);
		else
		{
			if(Refracmin>*(Density+i))
				Refracmin=*(Density+i);
		}	
	}

	return Refracmin;
}



