#include "Radiation.h"

double calcTotalRadiation(ImagePlane &plane, vector<Node> &nodes, vector<Unit> &units, double l1, double l2, double distance)
{
    double radiation = 0;
    for (int i = 0; i < plane.getN(); i++)
    {
        for (int j = 0; j < plane.getN(); j++)
        {
            if (!plane.units[i][j].indexOfUnit.empty())
            {
                Unit temp = units[plane.units[i][j].indexOfUnit.front()];
                double M = computeL1ToL2(l1, l2, temp.getT());

                double subArea = calcUnitProjectionArea(temp, nodes);
                double subRadiation = calcProjectedRadiation(M, subArea, distance);
                radiation += subRadiation;
            }
        }
    }

    return radiation;
}

double computeL1ToL2(double l1, double l2, double t)
{
    double lm = 2898 / t;
    double x1 = l1 / lm;
    double x2 = l2 / lm;
    double z1 = integral(fun, 0, x1, 10000);
    double z2 = integral(fun, 0, x2, 10000);
    double m = SIGAM * pow(t, 4) * (z2 - z1) / INFINITEZ;

    return m;
}

double calcUnitProjectionArea(Unit unit, vector<Node> &nodes)
{
    Node node[4];
    for (int i = 0; i < 4; i++)
    {
        node[i] = nodes[unit.getNode()[i]];
    }
    double a1 = calcTwoPointDistance(node[0], node[1]);
    double b1 = calcTwoPointDistance(node[1], node[2]);
    double c = calcTwoPointDistance(node[2], node[0]);
    double a2 = calcTwoPointDistance(node[0], node[3]);
    double b2 = calcTwoPointDistance(node[3], node[2]);
    double p1 = (a1 + b1 + c) / 2;
    double p2 = (a2 + b2 + c) / 2;
    double s1 = sqrt(p1*(p1 - a1)*(p1 - b1)*(p1 - c));
    double s2 = sqrt(p2*(p2 - a2)*(p2 - b2)*(p2 - c));

    return s1 + s2;
}

double calcProjectedRadiation(double M, double area, double l)
{
    return M * area / (PI * l * l);
}

double calcTwoPointDistance(Node node1, Node node2)
{
    double distance = sqrt(pow(node1.getX() - node2.getX(), 2) + pow(node1.getY() - node2.getY(), 2));
    return distance;
}

double integral(double(*f)(double x), double a, double b, int n)
{
    double step = (b - a) / n;
    double area = 0;
    for (int i = 0; i < n; i++)
    {
        area += f(a + (i + 0.5)*step) * step;
    }

    return area;
}

double fun(double x)
{
    return 142.32 / (pow(x, 5) * (exp(4.9651 / x) - 1));
}