#pragma once
#include <cmath>
#include "Grid.h"

using std::vector;

#define SIGAM 5.67E-8
#define B 1.2862E-11
#define INFINITEZ 1.520765205005

double calcTotalRadiation(ImagePlane &plane, vector<Node> &nodes, vector<Unit> &units, double l1, double l2, double distance);

double computeL1ToL2(double l1, double l2, double t);
double integral(double(*f)(double x), double a, double b, int n);
double fun(double x);   // 普朗克公式中的f(x)

double calcUnitProjectionArea(Unit unit, vector<Node> &nodes);
double calcProjectedRadiation(double M, double area, double l);
double calcTwoPointDistance(Node node1, Node node2);