#pragma once
#include <iostream>
#include <cmath>
#include <queue>
#define PI 3.1415926535898

class Node
{
private:
    double x, y, z; // 网格点坐标(x,y,z)
    int index; // 网格点编号
public:
    Node();
    Node(double x, double y, double z, int index);
    void setNode(double x, double y, double z, int index);
    double getX();
    double getY();
    double getZ();
    int getIndex();
};

class Unit
{
private:
    int node[4]; // 网格单元顶点的编号
    int index; // 网格单元编号
    double t;
public:
    Unit();
    Unit(int node1, int node2, int node3, int node4, int index, double t);
    void setUnit(int node1, int node2, int node3, int node4, int index, double t);
    int* getNode();
    int getIndex();
    double getT();
};

class ImageNode
{
private:
    double x, y; // 投影平面点坐标(x, y)
public:
    ImageNode();
    ImageNode(double x, double y);
    double getX();
    double getY();
};

class ImageUnit
{
private:
    ImageNode center;   // 网格单元中心点
    int i, j; // 网格单元坐标(i, j) i为行，j为列
public:
    std::queue<int> indexOfUnit; // 投影覆盖到本单元的表面网格单元的编号
    ImageUnit();
    ImageUnit(ImageNode center, int i, int j);
    double getCenterX();
    double getCenterY();
};

class ImagePlane
{
private:
    double viewDegree;  // 视场角（弧度）
    double l;   // 卫星-飞行器距离L
    double d0;  // 飞行器表面网格最小尺寸
    int n;   // 一条边上投影单元数，投影平面总单元数为n*n
    double sizeOfUnit; // 投影单元尺寸，l*viewDegree/n
public:
    ImageUnit** units;  // 投影平面网格，左下角为(0,0)
    ImagePlane();
    ImagePlane(double viewDegree, double l, double d0);
    void setImagePlane(double viewDegree, double l, double d0);
    int calcNumOfUnit(double l, double viewDegree, double d0);
    double calcSizeOfUnit(double l, double viewDegree, int n);
    ImageUnit** createUnits(int n, double sizeOfUnit);
    double getD0();
    int getN();
    double getSizeOfUnit();
};